# KUSTOMIZE - GRUPO DE CONOCIMIENTO - IAAC

## Pre-Requisitos

- Kustomize

  https://kubernetes-sigs.github.io/kustomize/installation/

  ```
  $ kustomize version
  {Version:3.6.1 GitCommit:c97fa946d576eb6ed559f17f2ac43b3b5a8d5dbd BuildDate:2020-05-28T01:07:36+01:00 GoOs:darwin GoArch:amd64}
  ```

---

## Crear un workspace

```
mkdir -p base/
mkdir -p overlays/dev/aks/configmaps
mkdir -p overlays/dev/aks/namespaces
mkdir -p overlays/dev/aks/patchs
mkdir -p overlays/dev/aks/secrets
```

```
├── bases
├── overlays
│   ├── dev
│   │   ├── aks
│   │   │   ├── configmaps
│   │   │   ├── namespaces
│   │   │   ├── patchs
│   │   │   ├── secrets
│   ├── cert
│   │   ├── aks
│   │   │   ├── configmaps
│   │   │   ├── namespaces
│   │   │   ├── patchs
│   │   │   ├── secrets
│   ├── prod
│   │   ├── aks
│   │   │   ├── configmaps
│   │   │   ├── namespaces
│   │   │   ├── patchs
│   │   │   ├── secrets
```

---

## Agregar los yaml necesarios para la carpeta base

- base/deployment.yaml

  ```
  cat <<EOF >>base/deployment.yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: hello-world
  spec:
    replicas: 1
    selector:
      matchLabels:
        app: hello-world
    revisionHistoryLimit: 3
    template:
      metadata:
        labels:
          app: hello-world
      spec:
        containers:
        - name: hello-world
          imagePullPolicy: Always
          ports:
          - containerPort: 8080
          resources:
            limits:
              cpu: 2
              memory: 2048M
            requests:
              cpu: 60m
              memory: 1024M
    strategy:
      type: RollingUpdate
      rollingUpdate:
        maxSurge: "25%"
        maxUnavailable: "25%"
  EOF
  ```

- base/service.yaml

  ```
  cat <<EOF >>base/service.yaml
  apiVersion: v1
  kind: Service
  metadata:
    name: hello-world
  spec:
    type: ClusterIP
    ports:
      - port: 8071
        targetPort: 8080
    selector:
      app: hello-world
  EOF
  ```

- base/hpa.yaml

  ```
  cat <<EOF >>base/hpa.yaml
  apiVersion: autoscaling/v2beta1
  kind: HorizontalPodAutoscaler
  metadata:
    name: hello-world
  spec:
    scaleTargetRef:
      apiVersion: extensions/v1beta1
      kind: Deployment
      name: hello-world
    minReplicas: 1
    maxReplicas: 4
    metrics:
    - type: Resource
      resource:
        name: cpu
        targetAverageUtilization: 2500
    - type: Resource
      resource:
        name: memory
        targetAverageUtilization: 98
  EOF
  ```

- base/ingress.yaml

  ```
  cat <<EOF >>base/ingress.yaml
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: hello-world
  spec:
    rules:
    - host: gitops.com
      http:
        paths:
        - backend:
            serviceName: hello-world
            servicePort: 8071
          path: /
  EOF
  ```

- base/kustomization.yaml

  ```
  cat <<EOF >>base/kustomization.yaml
  apiVersion: kustomize.config.k8s.io/v1beta1
  kind: Kustomization
  resources:
    - deployment.yaml
    - hpa.yaml
    - ingress.yaml
    - service.yaml
  EOF
  ```

---

## Agregar los yaml necesarios para la carpeta overlays

- overlays/dev/aks/configmaps/tests-v1-dev.yaml

  ```
  cat <<EOF >>overlays/dev/aks/configmaps/tests-v1-dev.yaml
  helloworld:
    mensajes:
      saludoInicial: "Hola buenas noches desarrollo"
  mundos:
    interaccion:
      saludos: "Hola somos del planeta tierra"
  infra-test:
    property-check: "Bienvenidos al grupo de conocimiento"
  EOF
  ```

- overlays/dev/aks/namespaces/gc-dev.yaml

  ```
  cat <<EOF >>overlays/dev/aks/namespaces/gc-dev.yaml
  apiVersion: v1
  kind: Namespace
  metadata:
    labels:
      name: gc-dev
    name: gc-dev
  EOF
  ```

- overlays/dev/aks/patchs/json-deploy.yaml

  ```
  cat <<EOF >>overlays/dev/aks/patchs/json-deploy.yaml
  - op: add
    path: /spec/template/spec/volumes
    value:
      - name: cm-helloworld
        configMap:
          name: cm-helloworld
  EOF
  ```

- overlays/dev/aks/patchs/json-ingress.yaml

  ```
  cat <<EOF >>overlays/dev/aks/patchs/json-ingress.yaml
  - op: replace
    path: /spec/rules/0/http/paths/0/path
    value: /helloworld-dev
  EOF
  ```

- overlays/dev/aks/patchs/merge-container.yaml

  ```
  cat <<EOF >>overlays/dev/aks/patchs/merge-container.yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: hello-world
  spec:
    template:
      spec:
        containers:
        - name: hello-world
          image: "droman08/gc-hello-world:1.0.0-SNAPSHOT-dev"
          env:
            - name: SPRING_PROFILES_ACTIVE
              value: "dev,swagger"
            - name: CONTAINER_SERVICE_PORT
              value: "8080"
            - name: SERVER_SERVLET_CONTEXT_PATH
              value: "/helloworld-dev"
            - name: SPRING_CONFIG_LOCATION
              value: file:/opt/security/configmaps/cm-helloworld/tests-v1-dev.yaml
          volumeMounts:
          - name: cm-helloworld
            mountPath: /opt/security/configmaps/cm-helloworld
  EOF
  ```

- overlays/dev/aks/patchs/merge-ingress.yaml

  ```
  cat <<EOF >>overlays/dev/aks/patchs/merge-ingress.yaml
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: hello-world
    annotations:
      kubernetes.io/ingress.class: nginx
  EOF
  ```

- overlays/dev/aks/kustomization.yaml

  ```
  cat <<EOF >>overlays/dev/aks/kustomization.yaml
  apiVersion: kustomize.config.k8s.io/v1beta1
  kind: Kustomization
  namespace: gc-dev
  commonAnnotations:
    environment: dev
    repository: https://gitlab.com/gc-gitops/helloworld.git
  commonLabels:
    app: hello-world
    version: 1.0.0
  bases:
    - ../../../base/
  resources:
    - namespaces/gc-dev.yaml
  configMapGenerator:
  - name: cm-helloworld
    files:
    - "configmaps/tests-v1-dev.yaml"
  secretGenerator:
  - name: root-cert
    files:
      - secrets/root-certificate.cer
  generatorOptions:
    disableNameSuffixHash: true
  patchesStrategicMerge:
    - patchs/merge-container.yaml
    - patchs/merge-ingress.yaml
  patchesJson6902:
    - target:
        group: apps
        version: v1
        kind: Deployment
        name: hello-world
      path: patchs/json-deploy.yaml
    - target:
        group: extensions
        version: v1beta1
        kind: Ingress
        name: hello-world
      path: patchs/json-ingress.yaml
  EOF
  ```

---

## Realizar el build del manifiesto

- Para la ejecución de la demo es necesario tener el siguiente archivo:

  overlays/dev/aks/secrets/root-certificate.cer

  Ejemplo: temp/root-certificate.cer

  ```
  kustomize build overlays/dev/aks/
  kubectl apply -k overlays/dev/aks/
  ```

---

## Clean

  ```
  kubectl delete ns flux gc-dev
  ```